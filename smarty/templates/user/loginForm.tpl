<form method="post" action="/user/login">
	<div class="form-group row">
		<label for="username" class="col-xs-12 col-sm-2 col-form-label">Username</label>
		<div class="col-xs-12 col-sm-3">
			<input name="username" class="form-control" type="text" value="" id="username">
		</div>
	</div>
	<div class="form-group row">
		<label for="password" class="col-xs-12 col-sm-2 col-form-label">Password</label>
		<div class="col-xs-12 col-sm-3">
			<input class="form-control" type="password" name="password" value="" id="password">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-primary" value="send">Send</button>
		</div>
	</div>
</form>