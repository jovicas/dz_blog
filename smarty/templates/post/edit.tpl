{*edit entry form*}
<form method="post" action="/post/editSavePost/postid/{$post.postid}">
    <div class="form-group row">
        <div class="col-xs-12">
            <h3>EDIT YOUR POST HERE: </h3>
        </div>
        <label for="author" class="col-xs-12 col-sm-2 col-form-label">Author: </label>
        <div class="col-xs-12 col-sm-8">
            {$post.username}
        </div>
    </div>
    <div class="form-group row">
        <label for="date" class="col-xs-12 col-sm-2 col-form-label">Create date: </label>
        <div class="col-xs-12 col-sm-8">
                {$post.create_date|date_format:"%d-%m-%Y"}
        </div>
    </div>
    <div class="form-group row">
        <label for="title" class="col-xs-12 col-sm-2 col-form-label">Title: </label>
        <div class="col-xs-12 col-sm-8">
                <input name="title" class="form-control" type="text" id="title" value="{$post.title}">
        </div>
    </div>
    <div class="form-group row">
        <label for="content" class="col-xs-12 col-sm-2 col-form-label">Content:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea rows="4" cols="250" name="content" class="form-control" id="content">{$post.content}</textarea>
        </div>
    </div>
    <div class="form-group row">
	<div class="col-xs-12">
            <button type="submit" class="btn btn-primary" value="send">Save Post</button>
	</div>
    </div>
</form>






{*<div class="col-xs-12">
		{$post.create_date|date_format:"%d-%m-%Y"}: <a href="/post/detail/postid/{$post.postid}">{$post.title}</a>
	   	{$post.content}
	   	<div class="row">
	    	<div class="col-xs-12 col-sm-2"><a href="/post/show/user/{$post.fk_user_id}">Autor: {$post.username}</a></div>
                <div class="col-xs-12 col-sm-3"><a href="/post/detail/postid/{$post.postid}">Comments:{$post.comments_count}</a></div>
	   	</div>
</div>*}
{*comments*}
{*{if $post.comments_count == 0}
    <h5>No comments yet</h5>
{else}*}
   {* <hr>*}
  {* <div class="col-xs-12">
    <h4><button class="btn btn-light">COMMENTS:</button></h4>
   </div>
    {foreach from=$comments item=comment}
        <div class="col-xs-12">
            <hr>
            <p>{$comment.comment}</p>
            <h6>
                ({$comment.commenter_name},
                {if $smarty.now|date_format:"%d-%m-%Y" === $comment.comment_date|date_format:"%d-%m-%Y"}
                    Today
                {else}
                    {$comment.comment_date|date_format:"%d-%m-%Y"}
                {/if})
            </h6>   	
        </div>
    {/foreach}
{/if}       *}
                
	