{foreach from=$posts item=post}
	<div class="col-xs-12">
            <h4>
                {if $smarty.now|date_format:"%d-%m-%Y" === $post.create_date|date_format:"%d-%m-%Y"}
                    Today
                {else}
                {$post.create_date|date_format:"%d-%m-%Y"}
                {/if}
                : <a href="/post/detail/postid/{$post.postid}">{$post.title}</a>
                <a href="/post/delete/postid/{$post.postid}/user/{$post.fk_user_id}"><button type="button" data-confirm="Are you sure you want to delete this post and all comments?" class="btn-xs btn-warning" id="delete-button">DELETE</button></a>
            </h4>
                <p>{$post.content|truncate:1000}
                    {if $post.contentLength > 1000}
                        <a href="/post/detail/postid/{$post.postid}">continue reading</a>
                    {/if}
                </p>
	   	<div class="row">
	    	<div class="col-xs-12 col-sm-2"><a href="/post/show/user/{$post.fk_user_id}">Autor: {$post.username}</a></div>
	    	<div class="col-xs-12 col-sm-3"><a href="/post/detail/postid/{$post.postid}">Comments:{$post.comments_count}</a></div>
	   	</div>
	</div>
{foreachelse}
	No blog entries yet
{/foreach}
<div class="row text-center pagination-centered">
{if $page != 1}
	<a href="/post/show/page/{$page-1}{if isset($user)}/user/{$user}{/if}"><button class="btn btn-primary" value="previous">Previous</button></a>
{/if}

{if $showNextLink}
	<a href="/post/show/page/{$page+1}{if isset($user)}/user/{$user}{/if}"><button class="btn btn-primary" value="next">Next</button></a>
{/if}
</div>