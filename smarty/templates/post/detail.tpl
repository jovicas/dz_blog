{*post entry*}
<div class="col-xs-12">
		<h4>{$post.create_date|date_format:"%d-%m-%Y"}: <a href="/post/detail/postid/{$post.postid}">{$post.title}</a>
                 <a href="/post/delete/postid/{$post.postid}/user/{$post.fk_user_id}"><button type="button" data-confirm="Are you sure you want to delete this post and all comments?" class="btn-xs btn-warning" id="delete-button">DELETE</button></a>
                  <a href="/post/editPost/postid/{$post.postid}/user/{$post.fk_user_id}/"><button type="button" class="btn-xs btn-secondary" id="edit-button">EDIT</button></a>
                </h4>
	   	<p>{$post.content}</p>
	   	<div class="row">
	    	<div class="col-xs-12 col-sm-2"><a href="/post/show/user/{$post.fk_user_id}">Autor: {$post.username}</a></div>
                <div class="col-xs-12 col-sm-3"><a href="/post/detail/postid/{$post.postid}">Comments:{$post.comments_count}</a></div>
	   	</div>
</div>
{*comments*}
{if $post.comments_count == 0}
    <h5>No comments yet</h5>
{else}
   {* <hr>*}
   <div class="col-xs-12">
    <h4><button class="btn btn-light">COMMENTS:</button></h4>
   </div>
    {foreach from=$comments item=comment}
        <div class="col-xs-12">
            <hr>
            <p>{$comment.comment}</p>
            <h3>
                ({$comment.commenter_name},
                {if $smarty.now|date_format:"%d-%m-%Y" === $comment.comment_date|date_format:"%d-%m-%Y"}
                    Today
                {else}
                    {$comment.comment_date|date_format:"%d-%m-%Y"}
                {/if})
            </h3>   	
        </div>
    {/foreach}
{/if}       
                
	