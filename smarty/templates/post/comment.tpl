{*comment form*}
<div>
<h5><button class="btn btn-block btn-warning">Leave a comment:</button></h5>
<form method="post" action="">
	<div class="form-group row">
		<label for="name" class="col-xs-12 col-sm-2 col-form-label">Name*:</label>
		<div class="col-xs-12 col-sm-6">
			<input name="name" value="{$commentInput.name}" class="form-control" type="text" id="name">
		</div>
        </div>
        <div class="form-group row">
                <label for="mail" class="col-xs-12 col-sm-2 col-form-label">Mail (optional):</label>
		<div class="col-xs-12 col-sm-6">
			<input name="mail" value="{$commentInput.mail}" class="form-control" type="email" id="mail">
		</div>
        </div>        
        <div class="form-group row">
                <label for="url" class="col-xs-12 col-sm-2 col-form-label">URL (optional):</label>
		<div class="col-xs-12 col-sm-6">
			<input name="url" value="{$commentInput.url}" class="form-control" type="url" id="url">
		</div>
	</div>
	<div class="form-group row">
		<label for="comment" class="col-xs-12 col-sm-2 col-form-label">Remark*:</label>
		<div class="col-xs-12 col-sm-8">
                    <textarea rows="4" cols="250" name="comment" class="form-control" id="comment">{$commentInput.comment}</textarea>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" name="submit" class="btn btn-warning" value="send">Send</button>
		</div>
	</div>
</form>
</div> 