<form method="post" action="">
	<div class="form-group row">
            <div class="col-xs-12">
            <h5>Leave Your post here</h5>
            </div>
		<label for="title" class="col-xs-12 col-sm-2 col-form-label">Title:</label>
		<div class="col-xs-12 col-sm-8">
			<input name="title" class="form-control" type="text" id="title">
		</div>
	</div>
	<div class="form-group row">
		<label for="content" class="col-xs-12 col-sm-2 col-form-label">Content:</label>
		<div class="col-xs-12 col-sm-8">
                    <textarea rows="4" cols="250" name="content" class="form-control" id="content"></textarea>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-primary" value="send">Add Post</button>
		</div>
	</div>
</form>