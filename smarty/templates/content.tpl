<div class="container">
   <div class="row">
   	<div class="col-xs-12">
   		<a href="/"><img class="img-responsive" src="/assets/img/img1.jpg" /></a>
   	</div>
   </div>
   <div class="container-fluid">
	   <div class="navbar">
		  <div class="navbar-inner">
		    <ul class="nav navbar-nav">
			  <li class="nav-item"><a href="/">Home-page</a></li>
			  {if $userId == null}
			  	<li class="nav-item"><a href="/user/login">Login</a></li>
			  {else}
			  	<li class="nav-item"><a href="/post/addEntry/">Add entry</a></li>
			  	<li class="nav-item"><a href="/user/logout">Logout</a></li>
			  {/if}
			</ul>
		  </div>
		</div>
	   	
	   	<div class="row">
	   		{if !empty($noticeMessage)}
	   			<div class="row">{$noticeMessage}</div>
	   		{/if}
	   		{$content}
	  	</div>
   		<div class="row">
			<div class="col-xs-12"><p class="text-center">© 2015 - <a href="imprint">Imprint</a></p></div>
		</div>
   </div>
</div>