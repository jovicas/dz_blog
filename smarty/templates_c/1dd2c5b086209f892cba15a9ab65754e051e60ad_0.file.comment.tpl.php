<?php
/* Smarty version 3.1.31, created on 2018-02-17 23:17:41
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\post\comment.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a88aa05e3e6e1_98011051',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1dd2c5b086209f892cba15a9ab65754e051e60ad' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\post\\comment.tpl',
      1 => 1518904684,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a88aa05e3e6e1_98011051 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div>
<h5><button class="btn btn-block btn-warning">Leave a comment:</button></h5>
<form method="post" action="">
	<div class="form-group row">
		<label for="name" class="col-xs-12 col-sm-2 col-form-label">Name*:</label>
		<div class="col-xs-12 col-sm-6">
			<input name="name" value="<?php echo $_smarty_tpl->tpl_vars['commentInput']->value['name'];?>
" class="form-control" type="text" id="name">
		</div>
        </div>
        <div class="form-group row">
                <label for="mail" class="col-xs-12 col-sm-2 col-form-label">Mail (optional):</label>
		<div class="col-xs-12 col-sm-6">
			<input name="mail" value="<?php echo $_smarty_tpl->tpl_vars['commentInput']->value['mail'];?>
" class="form-control" type="email" id="mail">
		</div>
        </div>        
        <div class="form-group row">
                <label for="url" class="col-xs-12 col-sm-2 col-form-label">URL (optional):</label>
		<div class="col-xs-12 col-sm-6">
			<input name="url" value="<?php echo $_smarty_tpl->tpl_vars['commentInput']->value['url'];?>
" class="form-control" type="url" id="url">
		</div>
	</div>
	<div class="form-group row">
		<label for="comment" class="col-xs-12 col-sm-2 col-form-label">Remark*:</label>
		<div class="col-xs-12 col-sm-8">
                    <textarea rows="4" cols="250" name="comment" class="form-control" id="comment"><?php echo $_smarty_tpl->tpl_vars['commentInput']->value['comment'];?>
</textarea>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" name="submit" class="btn btn-warning" value="send">Send</button>
		</div>
	</div>
</form>
</div> <?php }
}
