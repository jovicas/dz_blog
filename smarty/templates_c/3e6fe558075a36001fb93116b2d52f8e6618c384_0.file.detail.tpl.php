<?php
/* Smarty version 3.1.31, created on 2018-02-17 21:12:20
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\post\detail.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a888ca4eef519_14344716',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e6fe558075a36001fb93116b2d52f8e6618c384' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\post\\detail.tpl',
      1 => 1518898338,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a888ca4eef519_14344716 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\wamp\\www\\DIR\\DZ_BLOG\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php';
?>

<div class="col-xs-12">
		<h4><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['create_date'],"%d-%m-%Y");?>
: <a href="/post/detail/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['title'];?>
</a>
                 <a href="/post/delete/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
/user/<?php echo $_smarty_tpl->tpl_vars['post']->value['fk_user_id'];?>
"><button type="button" data-confirm="Are you sure you want to delete this post and all comments?" class="btn-xs btn-warning" id="delete-button">DELETE</button></a>
                  <a href="/post/editPost/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
/user/<?php echo $_smarty_tpl->tpl_vars['post']->value['fk_user_id'];?>
/"><button type="button" class="btn-xs btn-secondary" id="edit-button">EDIT</button></a>
                </h4>
	   	<p><?php echo $_smarty_tpl->tpl_vars['post']->value['content'];?>
</p>
	   	<div class="row">
	    	<div class="col-xs-12 col-sm-2"><a href="/post/show/user/<?php echo $_smarty_tpl->tpl_vars['post']->value['fk_user_id'];?>
">Autor: <?php echo $_smarty_tpl->tpl_vars['post']->value['username'];?>
</a></div>
                <div class="col-xs-12 col-sm-3"><a href="/post/detail/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
">Comments:<?php echo $_smarty_tpl->tpl_vars['post']->value['comments_count'];?>
</a></div>
	   	</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['post']->value['comments_count'] == 0) {?>
    <h5>No comments yet</h5>
<?php } else { ?>
   
   <div class="col-xs-12">
    <h4><button class="btn btn-light">COMMENTS:</button></h4>
   </div>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comments']->value, 'comment');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>
        <div class="col-xs-12">
            <hr>
            <p><?php echo $_smarty_tpl->tpl_vars['comment']->value['comment'];?>
</p>
            <h3>
                (<?php echo $_smarty_tpl->tpl_vars['comment']->value['commenter_name'];?>
,
                <?php if (smarty_modifier_date_format(time(),"%d-%m-%Y") === smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value['comment_date'],"%d-%m-%Y")) {?>
                    Today
                <?php } else { ?>
                    <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value['comment_date'],"%d-%m-%Y");?>

                <?php }?>)
            </h3>   	
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

<?php }?>       
                
	<?php }
}
