<?php
/* Smarty version 3.1.31, created on 2018-02-17 21:29:05
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\user\loginForm.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a889091125a20_03306078',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3649966a6f9232599e5ebf86fffd3a9407bff544' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\user\\loginForm.tpl',
      1 => 1518885571,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a889091125a20_03306078 (Smarty_Internal_Template $_smarty_tpl) {
?>
<form method="post" action="/user/login">
	<div class="form-group row">
		<label for="username" class="col-xs-12 col-sm-2 col-form-label">Username</label>
		<div class="col-xs-12 col-sm-3">
			<input name="username" class="form-control" type="text" value="" id="username">
		</div>
	</div>
	<div class="form-group row">
		<label for="password" class="col-xs-12 col-sm-2 col-form-label">Password</label>
		<div class="col-xs-12 col-sm-3">
			<input class="form-control" type="password" name="password" value="" id="password">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-primary" value="send">Send</button>
		</div>
	</div>
</form><?php }
}
