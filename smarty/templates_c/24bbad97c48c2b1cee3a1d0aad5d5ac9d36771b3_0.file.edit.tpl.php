<?php
/* Smarty version 3.1.31, created on 2018-02-17 22:15:55
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\post\edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a889b8bc87e25_40204855',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24bbad97c48c2b1cee3a1d0aad5d5ac9d36771b3' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\post\\edit.tpl',
      1 => 1518885572,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a889b8bc87e25_40204855 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\wamp\\www\\DIR\\DZ_BLOG\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php';
?>

<form method="post" action="/post/editSavePost/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
">
    <div class="form-group row">
        <div class="col-xs-12">
            <h3>EDIT YOUR POST HERE: </h3>
        </div>
        <label for="author" class="col-xs-12 col-sm-2 col-form-label">Author: </label>
        <div class="col-xs-12 col-sm-8">
            <?php echo $_smarty_tpl->tpl_vars['post']->value['username'];?>

        </div>
    </div>
    <div class="form-group row">
        <label for="date" class="col-xs-12 col-sm-2 col-form-label">Create date: </label>
        <div class="col-xs-12 col-sm-8">
                <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['create_date'],"%d-%m-%Y");?>

        </div>
    </div>
    <div class="form-group row">
        <label for="title" class="col-xs-12 col-sm-2 col-form-label">Title: </label>
        <div class="col-xs-12 col-sm-8">
                <input name="title" class="form-control" type="text" id="title" value="<?php echo $_smarty_tpl->tpl_vars['post']->value['title'];?>
">
        </div>
    </div>
    <div class="form-group row">
        <label for="content" class="col-xs-12 col-sm-2 col-form-label">Content:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea rows="4" cols="250" name="content" class="form-control" id="content"><?php echo $_smarty_tpl->tpl_vars['post']->value['content'];?>
</textarea>
        </div>
    </div>
    <div class="form-group row">
	<div class="col-xs-12">
            <button type="submit" class="btn btn-primary" value="send">Save Post</button>
	</div>
    </div>
</form>









   
  
                
	<?php }
}
