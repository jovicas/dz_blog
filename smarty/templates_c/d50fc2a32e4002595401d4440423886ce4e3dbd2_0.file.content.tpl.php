<?php
/* Smarty version 3.1.31, created on 2018-02-24 19:34:43
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\content.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a91b043ba68a1_83998431',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd50fc2a32e4002595401d4440423886ce4e3dbd2' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\content.tpl',
      1 => 1519481896,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a91b043ba68a1_83998431 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container">
   <div class="row">
   	<div class="col-xs-12">
   		<a href="/"><img class="img-responsive" src="/assets/img/img1.jpg" /></a>
   	</div>
   </div>
   <div class="container-fluid">
	   <div class="navbar">
		  <div class="navbar-inner">
		    <ul class="nav navbar-nav">
			  <li class="nav-item"><a href="/">Home-page</a></li>
			  <?php if ($_smarty_tpl->tpl_vars['userId']->value == null) {?>
			  	<li class="nav-item"><a href="/user/login">Login</a></li>
			  <?php } else { ?>
			  	<li class="nav-item"><a href="/post/addEntry/">Add entry</a></li>
			  	<li class="nav-item"><a href="/user/logout">Logout</a></li>
			  <?php }?>
			</ul>
		  </div>
		</div>
	   	
	   	<div class="row">
	   		<?php if (!empty($_smarty_tpl->tpl_vars['noticeMessage']->value)) {?>
	   			<div class="row"><?php echo $_smarty_tpl->tpl_vars['noticeMessage']->value;?>
</div>
	   		<?php }?>
	   		<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

	  	</div>
   		<div class="row">
			<div class="col-xs-12"><p class="text-center">© 2015 - <a href="imprint">Imprint</a></p></div>
		</div>
   </div>
</div><?php }
}
