<?php
/* Smarty version 3.1.31, created on 2018-02-17 22:28:42
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\post\add.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a889e8a01d2c0_08336498',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '065e98ecede7f0d4a981dc644230f7169d58c54d' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\post\\add.tpl',
      1 => 1518885572,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a889e8a01d2c0_08336498 (Smarty_Internal_Template $_smarty_tpl) {
?>
<form method="post" action="">
	<div class="form-group row">
            <div class="col-xs-12">
            <h5>Leave Your post here</h5>
            </div>
		<label for="title" class="col-xs-12 col-sm-2 col-form-label">Title:</label>
		<div class="col-xs-12 col-sm-8">
			<input name="title" class="form-control" type="text" id="title">
		</div>
	</div>
	<div class="form-group row">
		<label for="content" class="col-xs-12 col-sm-2 col-form-label">Content:</label>
		<div class="col-xs-12 col-sm-8">
                    <textarea rows="4" cols="250" name="content" class="form-control" id="content"></textarea>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-primary" value="send">Add Post</button>
		</div>
	</div>
</form><?php }
}
