<?php
/* Smarty version 3.1.31, created on 2018-02-24 19:34:43
  from "C:\wamp\www\DIR\DZ_BLOG\smarty\templates\post\list.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a91b043910022_70239113',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c27455b016e610311280dd232c01a70eb2abcf46' => 
    array (
      0 => 'C:\\wamp\\www\\DIR\\DZ_BLOG\\smarty\\templates\\post\\list.tpl',
      1 => 1519481905,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a91b043910022_70239113 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\wamp\\www\\DIR\\DZ_BLOG\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) require_once 'C:\\wamp\\www\\DIR\\DZ_BLOG\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.truncate.php';
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
	<div class="col-xs-12">
            <h4>
                <?php if (smarty_modifier_date_format(time(),"%d-%m-%Y") === smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['create_date'],"%d-%m-%Y")) {?>
                    Today
                <?php } else { ?>
                <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['create_date'],"%d-%m-%Y");?>

                <?php }?>
                : <a href="/post/detail/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
"><?php echo $_smarty_tpl->tpl_vars['post']->value['title'];?>
</a>
                <a href="/post/delete/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
/user/<?php echo $_smarty_tpl->tpl_vars['post']->value['fk_user_id'];?>
"><button type="button" data-confirm="Are you sure you want to delete this post and all comments?" class="btn-xs btn-warning" id="delete-button">DELETE</button></a>
            </h4>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['post']->value['content'],1000);?>

                    <?php if ($_smarty_tpl->tpl_vars['post']->value['contentLength'] > 1000) {?>
                        <a href="/post/detail/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
">continue reading</a>
                    <?php }?>
                </p>
	   	<div class="row">
	    	<div class="col-xs-12 col-sm-2"><a href="/post/show/user/<?php echo $_smarty_tpl->tpl_vars['post']->value['fk_user_id'];?>
">Autor: <?php echo $_smarty_tpl->tpl_vars['post']->value['username'];?>
</a></div>
	    	<div class="col-xs-12 col-sm-3"><a href="/post/detail/postid/<?php echo $_smarty_tpl->tpl_vars['post']->value['postid'];?>
">Comments:<?php echo $_smarty_tpl->tpl_vars['post']->value['comments_count'];?>
</a></div>
	   	</div>
	</div>
<?php
}
} else {
?>

	No blog entries yet
<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

<div class="row text-center pagination-centered">
<?php if ($_smarty_tpl->tpl_vars['page']->value != 1) {?>
	<a href="/post/show/page/<?php echo $_smarty_tpl->tpl_vars['page']->value-1;
if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>/user/<?php echo $_smarty_tpl->tpl_vars['user']->value;
}?>"><button class="btn btn-primary" value="previous">Previous</button></a>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['showNextLink']->value) {?>
	<a href="/post/show/page/<?php echo $_smarty_tpl->tpl_vars['page']->value+1;
if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>/user/<?php echo $_smarty_tpl->tpl_vars['user']->value;
}?>"><button class="btn btn-primary" value="next">Next</button></a>
<?php }?>
</div><?php }
}
