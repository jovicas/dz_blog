-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2018 at 03:50 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `check_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentid` int(11) NOT NULL AUTO_INCREMENT,
  `fk_post_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `commenter_name` varchar(50) NOT NULL,
  `commenter_email` varchar(255) NOT NULL,
  `commenter_url` varchar(512) NOT NULL,
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentid`, `fk_post_id`, `comment`, `commenter_name`, `commenter_email`, `commenter_url`, `comment_date`) VALUES
(48, 54, '1111111111111', '1111111111111111', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-03 20:27:54'),
(49, 54, '1111111111111', '1111111111111111', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-03 20:28:59'),
(50, 54, '1111111111111', '1111111111111111', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-03 20:29:48'),
(51, 157, 'omentar na xxxxxxxxxxxxxx', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-04 08:20:15'),
(53, 158, 'Komentar na Subotnji post by Dzaki\r\nKomentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar Komentar ', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-10 18:23:10'),
(55, 8, 'dddddddddddddddddd', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-10 22:20:55'),
(64, 171, '<p>xxxxxxxxxxxxxxxx</p>', 'prvi komentar', 'slavicadimic@yahoo.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 13:05:51'),
(65, 171, '<p>To ostaje da se vidi, ba&scaron; kao i ambiciozni planovi na&scaron;e vlade, jer te&scaron;ko je pametno postupiti kad ti se, odjednom, na?e mnogo para na gomili, a besparica i dalje vlada</p>\r\n<div class="ads">&nbsp;</div>\r\n<p>&nbsp;</p>\r\n<p>Posao godine ili &scaron;teta? Pa, možda, kad nam vrate aerodrom za 25 godina, budemo u stanju i mi sami da ga vodimo. Sad je tako kako je. Pesimisti ?e re?i da sledi ?etvrt veka robijanja i rintanja za strance u sopstvenoj državi, da je davanje u koncesiju &bdquo;Nikole Tesle&ldquo; proma&scaron;aj, ?ist gubitak. Optimisti: kako smo se nadali, dobro smo se i udali! Kapa?e parice!</p>', 'Milka Babovic', 'milka@bab.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:02:34'),
(66, 171, '<p>Peva? narodne muzike i autor mnogobrojnih hitova&nbsp;Milan?e Radosavljevi? (74), koji živi&nbsp;pored Obrenovca, u mladosti je doživeo klini?ku smrt.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Naime, kako prepri?avaju me&scaron;tani, svoje krive i neuobi?ajeno raspore?ene prste Milan?e tuma?i nesretnim slu?ajem koji je po mnogo ?emu nelogi?an. Njegove kolege, a i me&scaron;tani gde živi pri?aju da ga je grom udario u drvo ispod kojeg je sedeo.</p>', 'M.M', 'milka@bab.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:09:34'),
(67, 171, '<p>ssssssssssssssss</p>\r\n<p>dddddddddddddddd</p>\r\n<p>fffffffffffffffff</p>', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:17:48'),
(68, 171, '<p>sssss</p>\r\n<p>dddd</p>', 'perica', 'goran@topic.com', 'https://www.coolinarika.com/recept/961475/', '2018-02-17 20:27:34'),
(69, 158, '<p>fffffffffffff</p>', 'Goran Topic', 'goran@topic.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:30:54'),
(70, 160, '<p>cccccccccccccc</p>', 'Goran Topic', 'goran@topic.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:36:13'),
(71, 160, '<p>ggggggggggggggggggggggggggggggggggg</p>', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 20:41:36'),
(72, 160, '<p>xxxcccvvv</p>', 'Milica Vasic', 'milka@bab.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 21:16:45'),
(73, 8, '<p>ccccccccccccccccc</p>', 'Milica Vasic', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-17 21:25:23'),
(74, 171, '<p>komentar</p>', 'Jovica', 'jovica@mail.com', 'http://www.medfak.ni.ac.rs', '2018-02-22 19:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `postid` int(9) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(9) NOT NULL,
  `comments_count` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`postid`),
  KEY `fk_user_id` (`fk_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`postid`, `fk_user_id`, `comments_count`, `title`, `content`, `create_date`) VALUES
(1, 1, 0, 'Title 1. Dzakijevog posta ', 'vcsfdsnfkjs hsdfhskjfkjh hjdsfjkdsfkhjsfkhkhj', '2017-07-01 12:21:18'),
(2, 1, 0, 'Title 2. Dzakijevog posta ', '\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet eros finibus, dapibus justo et, congue nulla. Vestibulum feugiat tellus vitae arcu auctor sollicitudin. Aliquam ut elementum velit. Integer ullamcorper enim nec magna mattis tincidunt. Suspendisse nec nunc rutrum, malesuada ante sit amet, tincidunt massa. Vivamus finibus, erat eget fringilla scelerisque, sapien quam sollicitudin arcu, vel ullamcorper elit nisl et libero. Praesent lobortis, elit vitae gravida posuere, neque tellus dictum nibh, eget pharetra lorem quam quis mauris.\n\nMaecenas libero dui, viverra ut sem ac, mattis finibus metus. Nulla pellentesque, leo id suscipit vehicula, ipsum sem laoreet lacus, vitae dignissim dolor urna vitae urna. Nam ante metus, porta sed lorem eu, congue molestie quam. Vestibulum consectetur sem ut libero viverra dignissim. Sed non interdum arcu. Curabitur nec sem dolor. Praesent eu rhoncus odio, non fringilla ligula. In accumsan ultrices nisi, eu ultrices arcu aliquam sed. Ut et erat sit amet nunc molestie scelerisque. Nulla facilisi. Mauris in neque sed erat sagittis pretium non venenatis justo. Nam eget nulla ut nisl pharetra semper. Sed nec ex pellentesque felis laoreet faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sit amet venenatis lorem.\n\nAenean condimentum mattis ante consequat gravida. Maecenas dapibus magna scelerisque libero luctus sagittis. Quisque eu arcu sit amet elit venenatis tempus. Aliquam id mauris eu massa tincidunt commodo. Etiam at diam et sapien malesuada facilisis vel sit amet nisi. Sed a nunc neque. Integer at sagittis sem, in lobortis dolor. Etiam aliquam nibh sed placerat gravida. Nulla sit amet urna ac justo pellentesque gravida in sit amet risus. Integer eu placerat nisi. Aliquam posuere faucibus convallis. Ut aliquam cursus varius. Nullam tortor magna, varius a blandit sit amet, molestie sit amet velit. Pellentesque sit amet quam id lectus ultrices tristique ut vel purus. Curabitur sollicitudin ante et rhoncus varius.\n\nIn molestie porttitor mauris euismod tincidunt. Phasellus sagittis ut nulla sed congue. In aliquet dapibus augue quis mattis. Vivamus et tempor elit. Nulla viverra mi ac vehicula scelerisque. Vestibulum cursus malesuada accumsan. Donec ornare, velit vitae sagittis accumsan, nisl nisl tincidunt augue, vitae porta justo ligula sit amet mauris. Suspendisse potenti. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eu lorem in felis ultrices feugiat id nec lorem.\n\nMauris eget suscipit sapien. Sed rutrum pulvinar mauris sed pharetra. Quisque congue egestas tellus, gravida convallis dolor dignissim et. Praesent posuere ultrices odio, sed ultricies elit auctor ut. Sed id urna leo. Nulla malesuada euismod urna, non consequat tellus mattis iaculis. Nam luctus rhoncus sapien vel semper. Sed vehicula blandit purus ut mollis. ', '2017-07-02 05:16:00'),
(3, 1, 0, 'Title 3. Dzakijevog posta ', 'cxvxvc cxv cx vcx vcx v vcxx vcxvc cx ', '2017-07-03 03:04:07'),
(5, 2, 0, 'Title 1. Perinog posta ', 'comentar unet preko baze\r\nXXXXXXXXXXXXXXXXXXXXXXX', '2018-01-10 19:51:00'),
(6, 2, 0, 'Title 2. Perinog posta', 'comentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nncomentar unet preko azeYYY YYYYY YYYYY YYYYYYYYY nnn nnn nnn nn nnnn nnnn nnn nn', '2018-01-17 07:56:00'),
(7, 2, 0, 'Title 3. Perinog posta', 'Aenean condimentum mattis ante consequat gravida. Maecenas dapibus magna scelerisque libero luctus sagittis. Quisque eu arcu sit amet elit venenatis tempus. Aliquam id mauris eu massa tincidunt commodo. Etiam at diam et sapien malesuada facilisis vel sit amet nisi. Sed a nunc neque. Integer at sagittis sem, in lobortis dolor. Etiam aliquam nibh sed placerat gravida. Nulla sit amet urna ac justo pellentesque gravida in sit amet risus. Integer eu placerat nisi. Aliquam posuere faucibus convallis. Ut aliquam cursus varius. Nullam tortor magna, varius a blandit sit amet, molestie sit amet velit. Pellentesque sit amet quam id lectus ultrices tristique ut vel purus. Curabitur sollicitudin ante et rhoncus varius. Aenean condimentum mattis ante consequat gravida. Maecenas dapibus magna scelerisque libero luctus sagittis. Quisque eu arcu sit amet elit venenatis tempus. Aliquam id mauris eu massa tincidunt commodo. Etiam at diam et sapien malesuada facilisis vel sit amet nisi. Sed a nunc neque. Integer at sagittis sem, in lobortis dolor. Etiam aliquam nibh sed placerat gravida. Nulla sit amet urna ac justo pellentesque gravida in sit amet risus. Integer eu placerat nisi. Aliquam posuere faucibus convallis. Ut aliquam cursus varius. Nullam tortor magna, varius a blandit sit amet, molestie sit amet velit. Pellentesque sit amet quam id lectus ultrices tristique ut vel purus. Curabitur sollicitudin ante et rhoncus varius. ', '2018-01-17 20:33:34'),
(8, 3, 2, 'Title 1. Zikinog posta', 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ', '2018-01-27 07:40:49'),
(52, 1, 0, 'DZ 6', 'sesti post', '2018-02-03 20:19:03'),
(156, 1, 0, 'Najnoviji post!!!', 'vvvbbbvbbbbvnnnnnv', '2018-02-03 23:16:09'),
(157, 1, 1, 'xxx', 'xxx', '2018-02-04 08:19:14'),
(158, 1, 2, 'Subotnji post by Dzaki-IZMENJEN', '<p>IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, IZMENJEN, Subotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by DzakiSubotnji post by Dzaki</p>', '2018-02-10 18:22:37'),
(160, 2, 3, 'perin post xxxxxxxxxx', '<p>bbbbbbbbbbbbbbbb</p>', '2018-02-10 19:01:52'),
(171, 1, 6, 'dzakijev NEW post', '<p><strong><em>hhhh&nbsp; kkkkllll llll&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ooooo<br /></em></strong></p>', '2018-02-17 10:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `street` varchar(50) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `place` varchar(30) NOT NULL,
  PRIMARY KEY (`userid`),
  KEY `username` (`username`,`pass`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `name`, `surname`, `username`, `pass`, `user_email`, `street`, `postcode`, `place`) VALUES
(1, 'Ivan', 'Stankovic', 'dzaki', '20be65ca6db952375eac59fb469dc172', 'spasicjovica@hotmail.com', 'dsf', 'fds', 'sfd'),
(2, 'Pera', 'Peric', 'pera', 'c2dfa0fb30f42d1c8f6536d66ab8ab64', 'spasicjovica@hotmail.com', 'Pobedina', '18000', 'Nis'),
(3, 'Zika', 'Zikic', 'zika', '20be65ca6db952375eac59fb469dc172', 'spasicjovica@hotmail.com', 'Prvomajska 2', '18000', 'Nis'),
(4, 'Mika', 'Mikic', 'mika', '20be65ca6db952375eac59fb469dc172', 'spasicjovica@hotmail.com', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
