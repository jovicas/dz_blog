<?php 

session_start();

include_once(__DIR__ . "/vendor/autoload.php");
include_once(__DIR__ . "/config.php");

//PDO initialization 
try {
    $pdo = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASSWORD);
} catch(PDOException $e) {
    die("Failed to connect to the database: " . $e->getMessage());
}

//Smarty initialization
$smarty = new Smarty;
$smarty->setTemplateDir(__DIR__ . '/smarty/templates/');
$smarty->setCompileDir(__DIR__ . '/smarty/templates_c/');
// $smarty->SetCacheDir(__DIR__ . '/smarty/cache/');
// $smarty->SetConfigDir(__DIR__ . '/smarty/config/');


