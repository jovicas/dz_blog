<?php
include_once(__DIR__ . "/../bootstrap.php");

$router = new Check24\Router();

$controller = new Check24\FrontController($pdo, $smarty);

$controller->dispatch($router->getController(), $router->getAction(), $router->getParams());


