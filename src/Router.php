<?php
namespace Check24;

class Router
{
    private $request;

    private $controller = null;

    private $action = null;
    
    private $pageParams = null;


    public function __construct()
    {
        $this->request = trim($_SERVER['REQUEST_URI'], '/');
        $this->parseUrl();
    }

    public function parseUrl(){
         
        $urlArray = explode("/", $this->request);
         
        if (is_array($urlArray) && count($urlArray) > 1){
             
            $this->controller = ucfirst($urlArray[0]);
            	
            $this->action = $urlArray[1];

            //url params
            if (count($urlArray) > 2){
                
                $urlArray = array_slice($urlArray, 2);

                foreach ($urlArray as $key => $urlValue) {
                    if ($key % 2 == 0){
                        $urlParameter = $urlValue;
                    }else{
                        $paramsArray[$urlParameter] = $urlValue;
                    }
                }
                 
                $this->pageParams = $paramsArray;
                //print_r($paramsArray);
            }
        } else {
            $this->controller = $this->request;
            $this->action = 'index';
        }
    }

    public function getController(){
        return $this->controller;
    }

    public function getAction(){
        return $this->action;
    }

    public function getParams(){
        return $this->pageParams;
    }
}
