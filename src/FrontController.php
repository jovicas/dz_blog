<?php
namespace Check24;

class FrontController{

	private $controller;
	
	private $action;
	
	private $pdo;
	
	private $smarty;
	
	public function __construct($pdo, $smarty){
	    $this->pdo = $pdo;
	    $this->smarty = $smarty;
	}
	
	public function dispatch($controller, $action, $params){
		$className = "Check24\Controllers\\" . ucfirst($controller);
// 		var_dump($className);
		if (!class_exists($className)){
		    $className = "Check24\Controllers\\Home";
		    $action = "index";
		}

		$this->controller = new $className($this->pdo, $this->smarty);
		/**
                 * STAVLJENO JE DA, AKO NIJE DEFINISANA METODA,DA IDE NA METODU INDEX,
                 * ALI NJE NEMA U USER I POST KONTROLERU, PA SAM DODAO U TIM KONTROLERIMA
                 * public function index()(samo primera radi)
                 */
		if (!method_exists($this->controller, $action)){
		    $action = 'index';
		}
		
		$this->action = $action;
		
		ob_start();
		echo $this->controller->$action($params);
                //die();
		$this->controller->content = ob_get_contents();
		ob_end_clean();
		
		$this->controller->render();
	}
}

