<?php
namespace Check24\Controllers;

class Controller
{
    protected $smarty;
    
    protected $userId;
    
    private $pdo;
    
    public $content;
   //public $contentLengt;

    private $title;
    
    private $print = true;
    
    public function __construct($pdo, $smarty)
    {
        $this->pdo = $pdo;
        $this->smarty = $smarty;
        $this->isLoggedUser();
    }
    
    public function render()
    {
		ob_start();
		
		$this->header();
		$this->body();
		$this->footer();
		
		$output = ob_get_contents();
		ob_end_clean();
		
		if ($this->print){
			echo $output;
		}
		
		return $output;
    }
    
    public function header()
    {
        echo $this->smarty->display('header.tpl');
    }
    
    public function body()
    {          
        if (!empty($_SESSION['message'])) {
            $this->smarty->assign('noticeMessage', $_SESSION['message']);
            unset($_SESSION['message']);
        }
        $this->smarty->assign('userId', $this->userId);
        $this->smarty->assign('content', $this->content);
        echo $this->smarty->display('content.tpl');
    }
    
    public function footer()
    {
        echo $this->smarty->display('footer.tpl');
    }
    
    public function setPrint($value)
    {
        $this->print = $value;   
    }
    
    public function isLoggedUser()
    {
        if (!empty($_SESSION['user_id']) && $_SESSION['is_logged'] == 1) {
            $this->userId = $_SESSION['user_id'];
            echo 'Postoji user_id; User  je ulogovan!';
            return true;
        } else {
            $this->userId = null;
             echo 'NE postoji user_id; User  NIJE ulogovan!';
            return false;
        }
    }
}