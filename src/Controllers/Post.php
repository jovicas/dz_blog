<?php

namespace Check24\Controllers;
use Nette;

class Post extends Controller
{   
    public $commentInput = [];
    public function __construct($pdo, $smarty)
    {
        parent::__construct($pdo, $smarty);
        $this->postModule = new \Check24\Modules\Post($pdo);
    }
     /**
     * DA NE JAVLJA GRESKU, AKO NEMA METODE, VEC NEKO UKUCA SAMO KONTROLER POST 
     * ==> DA IDE NA POST-INDEX STRANU     * 
     */
    public function index()
    {
        $this->smarty->display('post/index.tpl');
    }
    
    public function show($params)
    {        
        if (!empty($params['page'])) {
            $page = $params['page'];
        } else {
            $page = 1;
        }        
        if (isset($params['user'])) {
            $posts = $this->postModule->getPostsByUser($params['user'], $page);
            $this->smarty->assign('user', $params['user']);
        } else {        
            $posts = $this->postModule->getPosts($page);
        }
        
        $showNextLink = $this->postModule->showNextLink($posts, $page);
        
        $this->smarty->assign('showNextLink', $showNextLink);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('posts', $posts);
        $this->smarty->display('post/list.tpl');
    }
    //sending mail function
    public function send_mail ($email, $sbj, $msg) {
        //PRAVLJENJE MAILA
	//require_once ('C:\wamp\www\DIR\DZ_BLOG\vendor\autoload.php');
	$mail = new Nette\Mail\Message;
	$mail->setFrom($email, 'Nette Mail')
	->addTo($email)
        //->addTo('jovicasp@gmail.com')
	->setSubject($sbj)
	->setBody($msg)
	//->addAttachment('img/ddd.jpg')
        ;
        //SLANJE MAILA kroz SMTP SERVER
        //use Nette\Mail\SmtpMailer;
	$mailer = new Nette\Mail\SmtpMailer([
		'host' => constant("HOST"),
		'username'=> constant("USERNAME"),
		'password'=> constant("PASSWORD"),
		'secure' => 'ssl',
		'context' =>  [
		'ssl' => [
		'capath' => '/path/to/my/trusted/ca/folder',
		],
		],
		]);
	$mailer->send($mail);
    }
    public function detail($params)
    {   
        $postid = $params['postid'];
        if(isset($_POST["submit"])) {
            $commentInput = [
                'comment'=>$_POST['comment'],
                'name'=>$_POST['name'],
                'mail'=>$_POST['mail'],
                'url'=>$_POST['url'], 
            ];
              $this->smarty->assign('commentInput', $commentInput);
            if (empty($_POST['name']) || empty($_POST['comment'])) {
                //assign postid and back to post/detail
                $_SESSION['message'] = "Name and Remark entry are mandatory!";
                $this->smarty->assign('postid', $postid);
                $this->smarty->display('post/comment.tpl');
            } else {
            //send mail to the author, insert comment in DB and show detail.tpl with comments
                /////////////////////////////////////////////////////////////////////////
                $post_0 = $this->postModule->getPost($postid);
                //author of the post must have user_email!
                $sbj ="Comment by ".$commentInput["name"]." (".$commentInput["mail"].") to post: ".$post_0["title"];
                //send e-mail to author with (every) new comment
                $this->send_mail($post_0["user_email"], $sbj, $commentInput["comment"]);
                ///////////////////////////////////////////////////////////////////////////
                $this->postModule->insertComment($postid, $commentInput);
                $commentInput = [];
                $this->smarty->assign('commentInput', $commentInput);
                $comments = $this->postModule->getComments($postid);
                $post = $this->postModule->getPost($postid);
                $this->smarty->assign('comments', $comments);
                $this->smarty->assign('post', $post);
                $this->smarty->display('post/detail.tpl');
                $this->smarty->display('post/comment.tpl');
            }
        } else {
            $comments = $this->postModule->getComments($postid);
            $post = $this->postModule->getPost($postid);
            $this->smarty->assign('comments', $comments);
            $this->smarty->assign('post', $post);
            $this->smarty->display('post/detail.tpl');
            $this->smarty->display('post/comment.tpl');
        }
    }
     public function addEntry()
    {   $userid = $this->userId;
        $title = $_POST['title'];
        $content = $_POST['content'];
        if (empty($userid)){
            //show login form
            $_SESSION['message'] = "You have to login first!";
            $this->smarty->display('user/loginForm.tpl');
        } else {
            if (empty($_POST['title']) || empty($_POST['content'])) {
                //back to Add Entry
                $_SESSION['message'] = "All entries are mandatory!";
                $this->smarty->display('post/add.tpl');                   
            } else {
                //enter post in DB
                $x=$this->postModule->insertPost($userid, $title, $content);
                header("Location: /post/detail/postid/$x");
                exit();      
            }
        }           
    }
     public function editPost($params)
    {   $userid = $this->userId;
        //userid is the id from the session!!!
        $postid = $params['postid'];
        $useridP = $params['user'];
        //useridP is the id from parametars!!!
        if (empty($userid)){
//            //show login form
            $_SESSION['message'] = "You have to login first!";
            $this->smarty->display('user/loginForm.tpl');
            //exit();
        } else {
            if ($useridP == $_SESSION['user_id']){
                $post = $this->postModule->getPost($postid);
                $this->smarty->assign('post', $post);
                $this->smarty->display('post/edit.tpl');
            } else {
                $_SESSION['message'] = "Only owner of the post can edit post!";               
            }                    
        }
    }
    public function editSavePost($params){
        //$userid = $this->userId;
        $postid = $params['postid'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        if (empty($_POST['title']) || empty($_POST['content'])) {
            //back to Edit Entry
            $_SESSION['message'] = "All entries are mandatory!";
            $post = $this->postModule->getPost($postid);
            $this->smarty->assign('post', $post);
            $this->smarty->display('post/edit.tpl');                   
        } else {
            //update post in DB
            $this->postModule->editSavePost($postid, $title, $content);
            header("Location: /post/detail/postid/$postid");
            exit();      
        }
    }
    public function delete($params)
    {
        $postid = $params['postid'];
        $userid = $params['user'];
        if ($userid == $_SESSION['user_id']){
            //delete post and all comments and back to index.php
            $this->postModule->delete($postid);
            $this->postModule->deleteComments($postid);           
            header("Location: /index.php");
            exit();
        }
        $_SESSION['message'] = "Only owner of the post can delete post and all comments!";
        //Ako vuce pogresno neki link iz templates_c. Obrises sve ispod templates_c sledecom komandom. 
        //$smarty->clearCompiledTemplate(); 
    }

}