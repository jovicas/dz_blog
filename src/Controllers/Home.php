<?php

namespace Check24\Controllers;

class Home extends Controller
{
    //public $contentLengt;
    public function __construct($pdo, $smarty)
    {
        parent::__construct($pdo, $smarty);
        
        $this->postModule = new \Check24\Modules\Post($pdo);
    }
    
    public function index()
    {
        $posts = $this->postModule->getPosts(1);
        $showNextLink = $this->postModule->showNextLink($posts, 1);
        
        $this->smarty->assign('showNextLink', $showNextLink);
        $this->smarty->assign('page', 1);
        $this->smarty->assign('posts', $posts);
        $this->smarty->display('post/list.tpl');
    }
}