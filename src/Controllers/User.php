<?php

namespace Check24\Controllers;

class User extends Controller
{
    public function __construct($pdo, $smarty)
    {
        parent::__construct($pdo, $smarty);
    
        $this->postModule = new \Check24\Modules\User($pdo);
    }
    /**
     * DA NE JAVLJA GRESKU, AKO NEMA METODE, VEC NEKO UKUCA SAMO KONTROLER USER 
     * ==> DA IDE NA USER-INDEX STRANU     * 
     */
    public function index()
    {
        $this->smarty->display('user/index.tpl');
    }
    public function login()
    {
        
        if (isset($_POST['username']) && isset($_POST['password'])) {
            //check credentials
            $valid = $this->postModule->checkCredentials($_POST['username'], $_POST['password']);
            //redirect to home
            if ($valid) {
                header("Location: /index.php");
                exit();
            } else {
                $_SESSION['message'] = "Login params are incorrect";
                $this->smarty->display('user/loginForm.tpl');
            }
        } else {
            //show login form
            $this->smarty->display('user/loginForm.tpl');
        }
    }
    
    public function logout()
    {
        
        unset($_SESSION['is_logged']);
        unset($_SESSION['user_id']);
        
        session_destroy();
        
        header("Location: /index.php");
        exit();
    }
}