<?php
namespace Check24\Modules;

class User extends Module
{
    private $tableName = 'users';

    public function checkCredentials($username, $pass)
    {
        $hash = md5($pass . CHECK_SALT);
        $stmt = $this->pdo->prepare("SELECT userid FROM " . $this->tableName . 
                                    " WHERE username=:username AND pass=:pass"); 
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':pass', $hash);
        $stmt->execute();
        
        $user = array();
        $user = $stmt->fetch();
        if (!empty($user)) {
            $_SESSION['user_id'] = $user['userid'];
            $_SESSION['is_logged'] = true;
            return true;
        } else {
            return false;
        }
    }
}