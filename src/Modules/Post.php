<?php
namespace Check24\Modules;

class Post extends Module
{
    private $tableName = 'posts';
    private $tableName2 = 'comments';
    
    private $limit = 3;
    public $contentLength;
    
    public function getPostsByUser($userId, $page)
    {
        $offset = ($page - 1) * $this->limit;
        
        $stmt = $this->pdo->prepare("SELECT SQL_CALC_FOUND_ROWS p.*, u.username, "
                                    . "CHAR_LENGTH(p.content)  AS 'contentLength' FROM " . $this->tableName .  
                                    " p INNER JOIN users u ON (u.userid = p.fk_user_id)" .
                                    " WHERE p.fk_user_id = :userId ORDER BY p.create_date DESC "
                                    . "LIMIT $offset, $this->limit");
        $stmt->bindValue(':userId', $userId);
        $stmt->execute();
        $posts = $stmt->fetchAll();
        return $posts;
    }
    
    public function getPosts($page)
    {   //OVO NIJE LIMIT OVO JE OFFSET TJ. OD KOG SE KRECE!!!
        //BILO PRE: $limit = ($page - 1) * $this->offset;
        $offset = ($page - 1) * $this->limit;
        
        $stmt = $this->pdo->prepare("SELECT SQL_CALC_FOUND_ROWS p.*, u.username, "
                                    . "CHAR_LENGTH(p.content)  AS 'contentLength' FROM " . $this->tableName .  
                                    " p INNER JOIN users u ON (u.userid = p.fk_user_id)" .
                                    " ORDER BY p.create_date DESC LIMIT $offset, $this->limit");
        $stmt->execute();
        $posts = $stmt->fetchAll();
        return $posts;
    }    
    public function getPost($postId)
    {
        $stmt = $this->pdo->prepare("SELECT p.*, u.username, u.user_email "
                                    . "FROM " . $this->tableName .
                                    " p INNER JOIN users u ON "
                                    . "(u.userid = p.fk_user_id) "
                                    . "WHERE p.postid = :postId");
        $stmt->bindValue(':postId', $postId);
        $stmt->execute();
        $post = $stmt->fetch();
        return $post;       
    }
    public function insertPost($userid,$title,$content)
    {
        $stmt = $this->pdo->prepare("INSERT INTO " . $this->tableName .  "
                                    (fk_user_id,title,content)
                                    VALUES (:fk_user_id,:title,:content)"); 
        $stmt->bindValue(':fk_user_id', $userid);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':content', $content);
        $stmt->execute();
        $postid = $this->pdo->lastInsertId();
        return $postid;
    }
    public function editSavePost($postid,$title,$content)
    {
        $stmt = $this->pdo->prepare("UPDATE " . $this->tableName .  
                                    " SET title =:title,content = :content 
                                    WHERE postid = :postId"); 
        $stmt->bindValue(':postId', $postid);
        $stmt->bindValue(':title', $title);
        $stmt->bindValue(':content', $content);
        $stmt->execute();
    }
    public function delete($postid)
    {
        $stmt = $this->pdo->prepare("DELETE FROM " . $this->tableName .
                                    " WHERE postid = :postid");
        $stmt->bindValue(':postid', $postid);
        $stmt->execute();
    }    
    public function showNextLink($posts, $page)
    {
        if (count($posts) < $this->limit) {
            return false;
        } elseif ($page * $this->limit < $this->getCounts()) {
            return true;
        } else {
            return false;
        }
    }
     public function getComments($postid)
    {   $stmt = $this->pdo->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM " 
                                    . $this->tableName2 . 
                                    " WHERE fk_post_id = :fk_post_id ORDER BY "
                                    . "comment_date DESC");
        $stmt->bindValue(':fk_post_id', $postid);
        $stmt->execute();
        $comments = $stmt->fetchAll();
        $commentsCount = $stmt->rowCount();
        $stmt1 = $this->pdo->prepare("UPDATE " . $this->tableName . 
                                    " SET comments_count=:comments_count"
                                    . " WHERE postid = :postid"); 
        $stmt1->bindValue(':comments_count', $commentsCount);
        $stmt1->bindValue(':postid', $postid);
        $stmt1->execute();
        return $comments;
    }
    public function insertComment($postid, $commentInput)
    {   $stmt = $this->pdo->prepare("INSERT INTO " . $this->tableName2 . 
                                    " (fk_post_id,comment,commenter_name,                                                           commenter_email,commenter_url) VALUES "
                                    . "(:fk_post_id,:comment,:commenter_name,"
                                    . ":commenter_email,:commenter_url)"); 
        $stmt->bindValue(':fk_post_id', $postid);
        $stmt->bindValue(':comment', $commentInput[comment]);
        $stmt->bindValue(':commenter_name', $commentInput[name]);
        $stmt->bindValue(':commenter_email', $commentInput[mail]);
        $stmt->bindValue(':commenter_url', $commentInput[url]);
        $stmt->execute();
    }
    public function deleteComments($postid)
    {   $stmt = $this->pdo->prepare("DELETE FROM " . $this->tableName2 .
                                    " WHERE fk_post_id = :fk_post_id"); 
        $stmt->bindValue(':fk_post_id', $postid);
        $stmt->execute();
    }
}