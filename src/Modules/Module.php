<?php

namespace Check24\Modules;

class Module
{
    protected $pdo;
    
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    
    public function getCounts()
    {
        $counts = $this->pdo->query('SELECT FOUND_ROWS();')->fetch(\PDO::FETCH_COLUMN);
    
        return $counts;
    }
}